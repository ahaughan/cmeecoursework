#!/usr/bin/python

"""Understanding global and local variables in functions"""
	
__author__ = 'Alice Haughan (alicehaughan@gmail.com)'
__version__ = '2.7.0'

## Try this first

_a_global = 10

def a_function():
	_a_global = 5
	_a_local = 4
	print "Inside the function, the value is ", _a_global
	print "Inside the function, the value is ", _a_local
	return None
	
a_function()
print "Outside the function, the value is ", _a_global

## Now try this

_a_global = 10

def a_function():
	global _a_global
	_a_global = 5
	_a_local = 4
	print "Inside the function, the value is ", _a_global
	print "Inside the function, the value is ", _a_local
	return None
	
a_function()
print "Outside the function, the value is ", _a_global

#!/usr/bin/python

"""Writing list comprehensions and coventional loops"""
	
__author__ = 'Alice Haughan (alicehaughan@gmail.com)'
__version__ = '2.7.0'

birds = ( ('Passerculus sandwichensis','Savannah sparrow',18.7),
          ('Delichon urbica','House martin',19),
          ('Junco phaeonotus','Yellow-eyed junco',19.5),
          ('Junco hyemalis','Dark-eyed junco',19.6),
          ('Tachycineata bicolor','Tree swallow',20.2),
         )

#(1) Write three separate list comprehensions that create three different
# lists containing the latin names, common names and mean body masses for
# each species in birds, respectively. 

# list of latin names
birds_lc = [x[0] for x in birds]
print birds_lc

# list of common names
birds_lc = [x[1] for x in birds]
print birds_lc

# list of mean body masses
birds_lc = [x[2] for x in birds]
print birds_lc

# (2) Now do the same using conventional loops (you can choose to do this 
# before 1 !). 

# conventional loop for making a list of latin names
latin_name = list()
for bird in birds:
	latin_name.append(bird[0])
print latin_name

# conventional loop for making a list of common names
latin_name = list()
for bird in birds:
	latin_name.append(bird[1])
print latin_name

# conventional loop for making a list of mean body masses
latin_name = list()
for bird in birds:
	latin_name.append(bird[2])
print latin_name

# ANNOTATE WHAT EVERY BLOCK OR, IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS
 


Week 2 - Introduction to Python

--Sandbox--

--Data--
ALL DATA IN SANDBOX

--Code-- plus description of script

basic_io.py = opens the text file, prints out the content, saves a list
	to a new file, stores an object in a dictionary 
	
basic_csv.py = import csv, read in a csv file, outputs a file containing 
	only specific parts of the input file
	
boilerplate.py = template for a script with imports and functions 

using_name.py = understanding the main argument

sysargv.py = understadning sysargv

scope.py = understanding global and local variables in functions 

control_flow.py = understanding the use of control statements	

cfexercises.py = trying for and while loops and defining functions

loops.py = examples of loops

oaks.py = combining loops into list comprehensions

lc2.py = using list comprehensions and conventional loops to pick out 
	parts of tuples that you are interested in
	
dictionary.py = writing to a dictionary in a specific order

tuple.py = printing out a desired output

align_seqs.py = not had the chance to finish yet!

Debugging scripts = had a quick go at but not had time to finish



	
	


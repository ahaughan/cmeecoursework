# This function calculates heights of trees from the angle of 
# elevation and the distance from the base using the trignometric
# formula height = distance * tan(radians)

# ARGUMENTS:
# degrees : The angle of elevation
# distance : The distance from base

# OUTPUT:
# The height of the tree, same units as "distance"

## PRACTICAL ONE
## Load trees.csv
MyData <- read.csv("../Data/trees.csv", header = TRUE)
## Calculate the tree heights for all trees in the data
## Use the original function and use a for loop to go through trees.csv
## to apply the function to each species
TreeHeight <- function(degrees, distance){
  radians <- degrees * pi/180
  height <- distance * tan(radians)
  print(paste(height))
  
  return(height)
}
# for each species/row in the csv file compute the tree height for each and append it to the table
Tree.Height.m = mapply(TreeHeight, MyData$Distance.m, MyData$Angle.degrees)

tmp = read.csv("../Results/MyData.csv") # write to a temp file
tmp = cbind(MyData, Tree.Height.m) # add heights as a column to MyData

write.csv(tmp, "../Results/TreeHts.csv") # write it to a csv file
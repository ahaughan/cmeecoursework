#!/bin/bash
# Author: Alice Haughan alicehaughan@gmail.com
# Script: csvtospace.sh
# Desc: takes a comma separated
#		values and converts it to a space separated values file
# Arguments: 1-> comma delimited file
# Date: Oct 2017

echo "Creating a space separated version of $1..."
cat $1 | tr -s "," "\s" >> $1.ssv

echo "Done!"

#exit
